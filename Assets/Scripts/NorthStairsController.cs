﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NorthStairsController : MonoBehaviour {

    // Use this for initialization
    void Start() {
        //set inactive. this object will be activated once the player collects pick ups
        this.gameObject.SetActive(false);
    }

    void Update() {
    }
}
