﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {
    public float Speed;
    public Text CountText;
    public Text WinText;

    //Hold reference to north stairs game object
    public GameObject NorthStairs;

    private Rigidbody _rb; //player rigidbody
    private int _count; //score

    /// <summary>
    /// This method is called first frame the script is active
    /// </summary>
    private void Start()
    {
        _rb = GetComponent<Rigidbody>(); // Gets and returns reference to attached rigid body, if there is one.

        _count = 0;
        DisplayScore();

        WinText.text = "";
    }

    /// <summary>
    /// This method is called before rendering a frame. Most of the code goes here.
    /// </summary>
    private void Update() 
    {
    }

    /// <summary>
    /// This method is called befor making any physics calculations.
    /// Any phsysics code goes here. Eg. Apply physics to the ball so it moves.
    /// </summary>
    private void FixedUpdate() 
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");
        float moveY = 0.0f;

        //jump if space is pressed
        if(Input.GetKeyDown(KeyCode.Space))
        {
            moveY = 17.5f;
        }

        Vector3 movement = new Vector3(moveHorizontal, moveY, moveVertical);

        _rb.AddForce(movement * Speed);
    }

    private void OnTriggerEnter(Collider other)
    {
        //exit if the other object is not tagged with Pick Up string
        if (!other.gameObject.CompareTag("Pick Up")) return;

        //if it is a Pick Up object...
        other.gameObject.SetActive(false);
        _count = _count + 1;
        DisplayScore();

        // Destroy(other.gameObject) 

        //if (other.gameObject.CompareTag("Pick Up"))
        //{
        //   other.gameObject.SetActive(false);
        //    _count = _count + 1;
        //    DisplayScore();
        //}
    }

    private void DisplayScore()
    {
        //Display score in UI
        CountText.text = "Score: " + _count.ToString();
        //If count is equal or bigger than 13, player collected all pick ups
        if (_count >= 13)
        {
            NorthStairs.gameObject.SetActive(true);
            WinText.text = "You Win";
        }
    }
}
