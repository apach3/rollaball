﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public GameObject Player;

    private Vector3 _offset;

    // Use this for initialization
    private void Start () {
        //find position offset between camera object and player object
	    _offset = transform.position - Player.transform.position;
	}

    /// <summary>
    /// Runs every frame as the Update function. But is guaranteed to have every calculation from Update finished.
    /// </summary>
    private void LateUpdate () {
        //every frame we set the position to position of the player + offset
        transform.position = Player.transform.position + _offset;
	}
}
